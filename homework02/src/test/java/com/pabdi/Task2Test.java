package com.pabdi;

import org.junit.Assert;
import org.junit.Test;

/**
 * Testing Task2. Calculation of Fibonacci and Factorial.
 */
public class Task2Test {

    /**
     * Test method checking correctness of Fibonacci value calculation
     * with for cycle.
     */
    @Test
    public void testFindFibonacciFor() {
        String method = "1";
        String value = "5";
        Assert.assertEquals(5, Task2.findFibonacci(method, value));
    }

    /**
     * Test method checking correctness of Fibonacci value calculation
     * with while cycle.
     */
    @Test
    public void testFindFibonacciWhile() {
        String method = "2";
        String value = "5";
        Assert.assertEquals(5, Task2.findFibonacci(method, value));
    }

    /**
     * Test method checking correctness of Factorial value calculation
     * with for cycle.
     */
    @Test
    public void testFindFactorialFor() {
        String method = "1";
        String value = "5";
        Assert.assertEquals(120, Task2.findFactorial(method, value));
    }

    /**
     * Test method checking correctness of Factorial value calculation
     * with while cycle.
     */
    @Test
    public void testFindFactorialWhile() {
        String method = "2";
        String value = "5";
        Assert.assertEquals(120, Task2.findFactorial(method, value));
    }

    /**
     * Test method checking that an exception is thrown
     * with certain parameter values
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindFibonacciIllegalArgumentException() {
        String method = "2";
        String value = "0.1";
        Task2.findFibonacci(method, value);
    }

    /**
     * Test method checking that an exception is thrown
     * with certain parameter values
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindFactorialIllegalArgumentException() {
        String method = "1";
        String value = "0.1";
        Task2.findFactorial(method, value);
    }
}