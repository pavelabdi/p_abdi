package com.pabdi;

import org.junit.Test;

import org.junit.Assert;

/**
 * Testing Task1. Calculation of G value.
 */
public class Task1Test {

    /**
     * Test method checking correctness of G value calculation.
     */
    @Test
    public void testCalculateG() {
        String[] args = {"2", "2", "1.5", "0.5"};
        Assert.assertEquals(39.4384, Task1.calculateG(args), 0.1);
    }

    /**
     * Test method checking that an exception is thrown
     * with certain parameter values.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCalculateGIllegalArgumentException() {
        String[] args = {"2.1", "2", "0.1", "0.5"};
        Task1.calculateG(args);
    }

    /**
     * Test method checking that an exception is thrown
     * with certain parameter values.
     */
    @Test(expected = ArithmeticException.class)
    public void testCalculateGArithmeticException() {
        String[] args = {"2", "0", "0", "0"};
        Task1.calculateG(args);
    }
}