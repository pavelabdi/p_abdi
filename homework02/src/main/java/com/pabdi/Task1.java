package com.pabdi;

/**
 * Task1. Calculation of G value.
 *
 * @author Pavel_Abdi hor
 */
public class Task1 {

    /**
     * The required number of arguments.
     */
    private static final int ARGUMENTS = 4;

    /**
     * The value of PI is {@value}.
     */
    private static final double PI = Math.PI;

    /**
     * Error message for incorrect argument.
     */
    private static final String INCORRECT_ARGUMENT = "Incorrect argument";

    /**
     * Error message for insufficient number of arguments.
     */
    private static final String ARGUMENTS_ERROR = "Not enough arguments";

    /**
     * Error message for division by zero.
     */
    private static final String DIVIDE_BY_ZERO = "Can't divide by zero";

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(final String[] args) {
        if (args.length < ARGUMENTS) {
            throw new ArrayIndexOutOfBoundsException(ARGUMENTS_ERROR);
        }
        System.out.println(calculateG(args));
    }

    /**
     * Method for G value calculation.
     *
     * @param args arguments
     * @return G value
     */
    public static double calculateG(final String[] args) {

        int a = parseInt(args[0]);
        int p = parseInt(args[1]);
        double m1 = parseDouble(args[2]);
        double m2 = parseDouble(args[3]);

        double denominator = Math.pow(p, 2) * (m1 + m2);
        if (denominator != 0) {
            return 4 * Math.pow(PI, 2) * Math.pow(a, 3) / denominator;
        } else {
            throw new ArithmeticException(DIVIDE_BY_ZERO);
        }
    }

    /**
     * Method for parse argument into int.
     *
     * @param arg actual argument
     * @return parsed argument
     */
    private static int parseInt(final String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(INCORRECT_ARGUMENT
                    + " " + arg, e);
        }
    }

    /**
     * Method for parse argument into double.
     *
     * @param arg actual argument
     * @return parsed argument
     */
    private static double parseDouble(final String arg) {
        try {
            return Double.parseDouble(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(INCORRECT_ARGUMENT
                    + " " + arg, e);
        }
    }
}
