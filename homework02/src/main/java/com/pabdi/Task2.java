package com.pabdi;

/**
 * Task2. Calculation of Fibonacci and Factorial.
 *
 * @author Pavel_Abdi hor
 */
public class Task2 {

    /**
     * The required number of arguments.
     */
    private static final int ARGUMENTS = 3;

    /**
     * The value for Fibonacci algorithm.
     */
    private static final String FIBONACCI = "1";

    /**
     * The value for Factorial algorithm.
     */
    private static final String FACTORIAL = "2";

    /**
     * The value for For cycle.
     */
    private static final String METHOD_FOR = "1";

    /**
     * The value for While cycle.
     */
    private static final String METHOD_WHILE = "2";

    /**
     * The first number of the Fibonacci sequence.
     */
    private static final int FIBONACCI_FIRST = 0;

    /**
     * The second number of the Fibonacci sequence.
     */
    private static final int FIBONACCI_SECOND = 1;

    /**
     * The 0! value.
     */
    private static final int FACTORIAL_ZERO = 1;

    /**
     * Error message for wrong method numbering.
     */
    private static final String INVALID_METHOD_ERROR = "Invalid method";

    /**
     * Error message for wrong algorithm numbering.
     */
    private static final String INVALID_ALGORITHM_ERROR = "Invalid algorithm";

    /**
     * Error message for insufficient number of arguments.
     */
    private static final String ARGUMENTS_ERROR = "Not enough arguments";

    /**
     * Error message for incorrect argument.
     */
    private static final String INCORRECT_ARGUMENT = "Incorrect argument";

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(final String[] args) {

        if (args.length < ARGUMENTS) {
            throw new ArrayIndexOutOfBoundsException(ARGUMENTS_ERROR);
        }

        String function = args[0];
        String method = args[1];
        String value = args[2];

        if (function.equals(FIBONACCI)) {
            findFibonacci(method, value);
        } else if (function.equals(FACTORIAL)) {
            findFactorial(method, value);
        } else {
            System.out.print(INVALID_ALGORITHM_ERROR);
        }
    }

    /**
     * Method to calculate Fibonacci.
     *
     * @param method method of calculation
     * @param value  how many numbers
     * @return Fibonacci number
     */
    public static int findFibonacci(final String method, final String value) {

        int number = parseInt(value);
        int fprev = FIBONACCI_FIRST;
        int fcurrent = FIBONACCI_SECOND;
        int fnext;

        if (number == 0) {
            fnext = FIBONACCI_FIRST;
        } else if (number == 1) {
            fnext = FIBONACCI_FIRST;
            System.out.println(fprev);
        } else if (number == 2) {
            fnext = FIBONACCI_SECOND;
            System.out.println(fprev + " " + fcurrent + " ");
        } else {
            fnext = fprev + fcurrent;
            System.out.print(fprev + " " + fcurrent + " ");
        }

        int index;

        if (method.equals(METHOD_FOR)) {
            for (index = 3; index <= number; index++) {
                System.out.print(fnext + " ");
                fprev = fcurrent;
                fcurrent = fnext;
                fnext = fprev + fcurrent;
            }
        } else if (method.equals(METHOD_WHILE)) {
            index = 3;
            while (index <= number) {
                System.out.print(fnext + " ");
                fprev = fcurrent;
                fcurrent = fnext;
                fnext = fprev + fcurrent;
                index++;
            }
        } else {
            System.out.println(INVALID_METHOD_ERROR);
        }
        return fnext;
    }

    /**
     * Method to calculate Factorial.
     *
     * @param method method of calculation
     * @param value  how many numbers
     * @return Factorial value
     */
    public static int findFactorial(final String method, final String value) {

        int number = parseInt(value);
        int fac = FACTORIAL_ZERO;
        int index;

        if (method.equals(METHOD_FOR)) {
            for (index = 1; index <= number; index++) {
                fac = fac * index;
                System.out.print(fac + " ");
            }
        } else if (method.equals(METHOD_WHILE)) {
            index = 1;
            while (index <= number) {
                fac = fac * index;
                index++;
                System.out.print(fac + " ");
            }
        } else {
            System.out.print(INVALID_METHOD_ERROR);
        }
        return fac;
    }

    /**
     * Method for parse argument into int.
     *
     * @param arg actual argument
     * @return parsed argument
     */
    private static int parseInt(final String arg) {
        try {
            return Integer.parseUnsignedInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(INCORRECT_ARGUMENT
                    + " " + arg, e);
        }
    }
}
