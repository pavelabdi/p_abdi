package com.pabdi.task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Testing SelectionSorter. Selection sorting algorithm.
 */
public class SortingServiceTest {

    /**
     * Test method checking Bubble sort method.
     */
    @Test
    public void testPerformSortBubble() {
        String line = "5 6 8 1 4 4 20";
        String method = "1";
        int[] sortedArray = SortingService.performSort(line, method);
        Assert.assertTrue(Arrays.equals(sortedArray, new int[]{1, 4, 4, 5, 6, 8, 20}));
    }

    /**
     * Test method checking Selection sort method.
     */
    @Test
    public void testPerformSortSelection() {
        String line = "6 6 9 1 4 4";
        String method = "2";
        int[] sortedArray = SortingService.performSort(line, method);
        Assert.assertTrue(Arrays.equals(sortedArray, new int[]{1, 4, 4, 6, 6, 9}));
    }

    /**
     * Test method checking that an exception is thrown
     * with incorrect array values.
     */
    @Test(expected = NumberFormatException.class)
    public void testPerformSortNumberFormatException() {
        String line = "6.5 6 9 1 4 4";
        String method = "2";
        SortingService.performSort(line, method);
    }

    /**
     * Test method checking that an exception is thrown
     * with incorrect algorithm number.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPerformSortIllegalArgumentException() {
        String line = "6 6 9 1 4 4";
        String method = "10";
        SortingService.performSort(line, method);
    }
}
