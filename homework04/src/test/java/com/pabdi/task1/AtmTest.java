package com.pabdi.task1;

import com.pabdi.task1.impl.CreditCard;
import com.pabdi.task1.impl.DebitCard;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AtmTest {

    private Atm atm = new Atm();

    /**
     * Test method checking that amount is correctly
     * added to the credit card.
     */
    @Test
    public void addToCreditCard() {
        Card card = new CreditCard("John Doe", new BigDecimal(100));
        atm.setCard(card);
        BigDecimal balance = atm.addToCard(new BigDecimal(10));
        BigDecimal expectedValue = new BigDecimal(110).setScale(2, RoundingMode.HALF_UP);
        Assert.assertEquals(expectedValue, balance);
    }

    /**
     * Test method checking that amount is correctly
     * withdrawn from the credit card.
     */
    @Test
    public void withdrawFromCreditCard() {
        Card card = new CreditCard("John Doe", new BigDecimal(0));
        atm.setCard(card);
        BigDecimal balance = atm.withdrawFromCard(new BigDecimal(10));
        BigDecimal expectedValue = new BigDecimal(-10).setScale(2, RoundingMode.HALF_UP);
        Assert.assertEquals(expectedValue, balance);
    }

    /**
     * Test method checking that amount is correctly
     * added to the debit card.
     */
    @Test
    public void addToDebitCard() {
        Card card = new DebitCard("John Doe", new BigDecimal(100));
        atm.setCard(card);
        BigDecimal balance = atm.addToCard(new BigDecimal(10));
        BigDecimal expectedValue = new BigDecimal(110).setScale(2, RoundingMode.HALF_UP);
        Assert.assertEquals(expectedValue, balance);
    }

    /**
     * Test method checking that amount is correctly
     * withdrawn from the debit card.
     */
    @Test
    public void withdrawFromDebitCard() {
        Card card = new DebitCard("John Doe", new BigDecimal(100));
        atm.setCard(card);
        BigDecimal balance = atm.withdrawFromCard(new BigDecimal(10));
        BigDecimal expectedValue = new BigDecimal(90).setScale(2, RoundingMode.HALF_UP);
        Assert.assertEquals(expectedValue, balance);
    }

    /**
     * Test method checking that an exception is thrown
     * when there are insufficient funds on the debit card.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void withdrawFromDebitCardUnsupportedOperationException() {
        Card card = new DebitCard("John Doe", new BigDecimal(0));
        atm.setCard(card);
        atm.withdrawFromCard(new BigDecimal(10));
    }
}