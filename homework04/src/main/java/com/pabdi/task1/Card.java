package com.pabdi.task1;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Card class to emulate card behavior.
 *
 * @author Pavel_Abdi hor
 */
public abstract class Card {

    /**
     * Card holder name.
     */
    private String cardHolderName;

    /**
     * Card account balance.
     */
    private BigDecimal accountBalance;

    /**
     * Rounding mode to round towards "nearest neighbor"
     * unless both neighbors are equidistant,
     * in which case round up.
     */
    protected final RoundingMode round = RoundingMode.HALF_UP;

    /**
     * Number of decimal places.
     */
    protected final int accuracy = 2;

    /**
     * Constructor accepting card holder name and account balance.
     *
     * @param cardHolderName Card holder name
     * @param accountBalance account balance
     */
    public Card(final String cardHolderName, final BigDecimal accountBalance) {
        this.cardHolderName = cardHolderName;
        this.accountBalance = accountBalance.setScale(accuracy, round);
    }

    /**
     * Constructor accepting card holder name.
     *
     * @param cardHolderName Card holder name
     */
    public Card(final String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    /**
     * Constructor without parameters.
     */
    public Card() {

    }

    /**
     * Method to get the card holder name.
     *
     * @return Card holder name
     */
    public String getCardHolderName() {
        return this.cardHolderName;
    }

    /**
     * Method to set the card holder name.
     *
     * @param cardHolderName Card account balance
     */
    public void setCardHolderName(final String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    /**
     * Method to get the card account balance.
     *
     * @return Card account balance
     */
    public BigDecimal getAccountBalance() {
        return this.accountBalance;
    }

    /**
     * Method to set the card account balance.
     *
     * @param accountBalance Card account balance
     */
    public void setAccountBalance(final BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    /**
     * Method to add an amount to the card account.
     *
     * @param amount amount to be added to the card account
     * @return Card account balance after adding the amount
     */
    public BigDecimal addToAccount(final BigDecimal amount) {
        amount.setScale(accuracy, round);
        this.accountBalance = this.accountBalance.add(amount);
        return this.accountBalance;
    }

    /**
     * Method to withdraw an amount from the card account.
     *
     * @param amount amount to be withdrawn from the card account
     * @return Card account balance after withdrawing the amount
     */
    public abstract BigDecimal withdrawFromAccount(final BigDecimal amount);

    /**
     * Method to get the account balance converted into another currency.
     *
     * @param rate currency exchange rate
     * @return Card account balance
     * converted into another currency
     */
    public BigDecimal getAccountBalanceConverted(final double rate) {
        BigDecimal rateBigDecimal = new BigDecimal(rate);
        return this.accountBalance.divide(rateBigDecimal, accuracy, round);
    }
}
