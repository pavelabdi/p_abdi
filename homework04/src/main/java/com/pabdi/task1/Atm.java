package com.pabdi.task1;

import java.math.BigDecimal;

/**
 * Card class to emulate ATM behavior.
 *
 * @author Pavel_Abdi hor
 */
public class Atm {

    /**
     * Inserted card.
     */
    private Card card;

    /**
     * Method to get the inserted card.
     *
     * @return Card
     */
    public Card getCard() {
        return card;
    }

    /**
     * Method to set the inserted card.
     *
     * @param card Card to insert
     */
    public void setCard(final Card card) {
        this.card = card;
    }

    /**
     * Method to add money to the account.
     *
     * @param amount Money to add
     * @return Account balance after the operation
     */
    public BigDecimal addToCard(final BigDecimal amount) {
        return card.addToAccount(amount);
    }

    /**
     * Method to withdraw money to the account.
     *
     * @param amount Money to withdraw
     * @return Account balance after the operation
     */
    public BigDecimal withdrawFromCard(final BigDecimal amount) {
        return card.withdrawFromAccount(amount);
    }
}
