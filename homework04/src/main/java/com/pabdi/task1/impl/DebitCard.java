package com.pabdi.task1.impl;

import com.pabdi.task1.Card;

import java.math.BigDecimal;

/**
 * Class to emulate debit card behavior.
 *
 * @author Pavel_Abdi hor
 */
public class DebitCard extends Card {

    /**
     * Error message when withdrawal leads to negative balance.
     */
    private static final String WITHDRAW_ERROR = "Insufficient funds";

    /**
     * Constructor accepting debit card holder name and account balance.
     *
     * @param cardHolderName Debit card holder name
     * @param accountBalance account balance
     */
    public DebitCard(final String cardHolderName,
                     final BigDecimal accountBalance) {
        this.setCardHolderName(cardHolderName);
        this.setAccountBalance(accountBalance.setScale(accuracy, round));
    }

    /**
     * Constructor accepting debit card holder name.
     *
     * @param cardHolderName Debit card holder name
     */
    public DebitCard(final String cardHolderName) {
        this.setCardHolderName(cardHolderName);
    }

    /**
     * Constructor without parameters.
     */
    public DebitCard() {
    }

    /**
     * Method to withdraw an amount from the debit card account.
     *
     * @param amount amount to be withdrawn from the debit card account
     * @return Debit card account balance after withdrawing the amount
     */
    @Override
    public BigDecimal withdrawFromAccount(final BigDecimal amount) {
        BigDecimal currentAccountBalance = getAccountBalance();
        amount.setScale(accuracy, round);
        if (currentAccountBalance.subtract(amount).signum() < 0) {
            throw new UnsupportedOperationException(WITHDRAW_ERROR);
        } else {
            setAccountBalance(currentAccountBalance.subtract(amount));
            return getAccountBalance();
        }
    }
}
