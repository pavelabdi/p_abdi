package com.pabdi.task1.impl;

import com.pabdi.task1.Card;

import java.math.BigDecimal;

/**
 * Class to emulate credit card behavior.
 *
 * @author Pavel_Abdi hor
 */
public class CreditCard extends Card {

    /**
     * Constructor accepting credit card holder name and account balance.
     *
     * @param cardHolderName Credit card holder name
     * @param accountBalance account balance
     */
    public CreditCard(final String cardHolderName,
                      final BigDecimal accountBalance) {
        this.setCardHolderName(cardHolderName);
        this.setAccountBalance(accountBalance.setScale(accuracy, round));
    }

    /**
     * Constructor accepting credit card holder name.
     *
     * @param cardHolderName Credit card holder name
     */
    public CreditCard(final String cardHolderName) {
        this.setCardHolderName(cardHolderName);
    }

    /**
     * Constructor without parameters.
     */
    public CreditCard() {
    }

    /**
     * Method to withdraw an amount from the credit card account.
     *
     * @param amount amount to be withdrawn from the credit card account
     * @return Credit card account balance after withdrawing the amount
     */
    @Override
    public BigDecimal withdrawFromAccount(final BigDecimal amount) {
        BigDecimal currentAccountBalance = getAccountBalance();
        amount.setScale(accuracy, round);
        setAccountBalance(currentAccountBalance.subtract(amount));
        return getAccountBalance();
    }
}
