package com.pabdi.task2;

import com.pabdi.task2.impl.BubbleSorter;
import com.pabdi.task2.impl.SelectionSorter;

import java.util.Arrays;
import java.util.Scanner;

/**
 * SortingService. Sorting of int array.
 *
 * @author Pavel_Abdi hor
 */
public class SortingService {

    /**
     * Sorting context.
     */
    private static SortingContext context = new SortingContext();

    /**
     * Scanner to read console input.
     */
    private static Scanner sc = new Scanner(System.in);

    /**
     * Number of Bubble sorting algorithm.
     */
    private static final String BUBBLE = "1";

    /**
     * Number of Select sorting algorithm.
     */
    private static final String SELECTION = "2";

    /**
     * Error message for incorrect array values.
     */
    private static final String PARSE_ERROR = "Non integer values entered";

    /**
     * Error message for incorrect algorithm selection.
     */
    private static final String ALGORITHM_ERROR = "Wrong algorithm number";

    /**
     * Method for parse argument into int.
     *
     * @param line string input from the console
     * @return array of int
     */
    private static int[] parseString(final String line) {
        try {
            return Arrays.stream(line.split("\\s+"))
                    .mapToInt(Integer::parseInt)
                    .toArray();
        } catch (Exception e) {
            throw new NumberFormatException(PARSE_ERROR);
        }
    }

    /**
     * Method to perform sorting operation.
     *
     * @param line values input from the console
     * @param method sorting method input from the console
     * @return array of int
     */
    public static int[] performSort(final String line, final String method) {

        int[] array = parseString(line);

        if (method.equals(BUBBLE)) {
            context.setSorter(new BubbleSorter());
        } else if (method.equals(SELECTION)) {
            context.setSorter(new SelectionSorter());
        } else {
            throw new IllegalArgumentException(ALGORITHM_ERROR);
        }

        context.execute(array);
        return array;
    }

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(final String[] args) {

        System.out.println("Enter integer numbers separated by white spaces");
        String line = sc.nextLine();


        System.out.println("Choose sorting algorithm: "
                + "1 for Bubble; 2 for Selection");
        String choice = sc.next();

        int[] sortedArray = performSort(line, choice);

        System.out.println(Arrays.toString(sortedArray));
    }
}
