package com.pabdi.task2;

/**
 * SortingContext. Sorting of int array.
 *
 * @author Pavel_Abdi hor
 */
public class SortingContext {

    /**
     * Sorting algorithm.
     */
    private Sorter sorter;

    /**
     * Method to get the sorting algorithm.
     *
     * @return Sorting algorithm
     */
    public Sorter getSorter() {
        return sorter;
    }

    /**
     * Method to set the desired sorting algorithm.
     *
     * @param sorter Sorting algorithm
     */
    public void setSorter(final Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * Method to perform sorting operation.
     *
     * @param array Array to sort
     */
    public void execute(final int[] array) {
        sorter.sort(array);
    }
}
