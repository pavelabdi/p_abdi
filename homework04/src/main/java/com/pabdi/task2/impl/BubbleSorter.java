package com.pabdi.task2.impl;

import com.pabdi.task2.Sorter;

/**
 * Bubble sorter class.
 */
public class BubbleSorter implements Sorter {

    /**
     * Method to sort array of int using Bubble sort.
     *
     * @param array actual argument
     */
    @Override
    public void sort(final int[] array) {
        boolean sorted = false;
        int temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    sorted = false;
                }
            }
        }
    }
}
