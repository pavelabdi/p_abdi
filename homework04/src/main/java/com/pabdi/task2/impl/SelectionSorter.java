package com.pabdi.task2.impl;

import com.pabdi.task2.Sorter;

/**
 * Selection sorter class.
 */
public class SelectionSorter implements Sorter {

    /**
     * Method to sort array of int using Selection sort.
     *
     * @param array actual argument
     */
    @Override
    public void sort(final int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = j;
                }
            }

            int temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
    }
}
