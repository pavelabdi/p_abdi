package com.pabdi.task2;

/**
 * Sorting algorithm interface.
 *
 * @author Pavel_Abdi hor
 */
public interface Sorter {

    /**
     * Method to sort an array.
     *
     * @param array Array to sort
     */
    void sort(int[] array);
}
